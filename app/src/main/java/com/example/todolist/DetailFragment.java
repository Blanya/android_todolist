package com.example.todolist;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.todolist.databinding.FragmentDetailBinding;
import com.example.todolist.model.Task;
import com.example.todolist.service.TaskService;

public class DetailFragment extends Fragment {

  FragmentDetailBinding binding;
  private int id;
  private TaskService taskService;
  private Task task;

    public DetailFragment() {
        // Required empty public constructor
        taskService = new TaskService();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            DetailFragmentArgs args = DetailFragmentArgs.fromBundle(getArguments());
            task = taskService.getTaskByIndex(args.getId());
            id = args.getId();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentDetailBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        binding.titleTextView.setText(task.getTitle());
        String state = task.getDone()? "Done": "To do";
        binding.stateTextView.setText(state);
        if(task.getDescription() != null)
            binding.descriptionTextView.setText(task.getDescription());

        binding.deleteButton.setOnClickListener((e) -> {
            taskService.deleteTask(id);
            NavHostFragment.findNavController(DetailFragment.this).popBackStack();
        });
    }
}