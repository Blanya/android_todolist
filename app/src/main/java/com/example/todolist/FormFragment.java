package com.example.todolist;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.NavHostFragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.todolist.databinding.FragmentFormBinding;
import com.example.todolist.model.Task;
import com.example.todolist.service.TaskService;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class FormFragment extends Fragment {

    private FragmentFormBinding binding;
    private Task task;
    private TaskService taskService;

    public FormFragment() {
        taskService = new TaskService();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentFormBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        binding.submitTask.setOnClickListener(e -> {
            //get date
            String day = String.valueOf(binding.dateTask.getDayOfMonth());
            String month = String.valueOf(binding.dateTask.getMonth() + 1);
            String year = String.valueOf(binding.dateTask.getYear());
            Date date;
            try {
                date = new SimpleDateFormat("dd/MM/yyyy").parse(day + "/" + month + "/" + year);
                if(date.before(new Date())){
                    date = null;
                }
            }catch (ParseException exception){
                date = null;
            }
            if(!binding.descriptionTask.getText().toString().isBlank())
                task = new Task(binding.titleTask.getText().toString(), date, binding.descriptionTask.getText().toString());
            else
                task = new Task(binding.titleTask.getText().toString(), date);
            taskService.add(task);

            binding.titleTask.setText(null);
            NavDirections navDirections = FormFragmentDirections.actionFormFragmentToListFragment();
            NavHostFragment.findNavController(FormFragment.this).navigate(navDirections);

            binding.titleTask.setText(null);
            binding.descriptionTask.setText(null);
        });

    }
}