package com.example.todolist;

import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import com.example.todolist.adapter.TasksAdapter;
import com.example.todolist.databinding.FragmentListBinding;
import com.example.todolist.service.TaskService;
public class ListFragment extends Fragment {

    private FragmentListBinding binding;
    private TaskService taskService;
    private TasksAdapter tasksAdapter;

    public ListFragment() {
        taskService = new TaskService();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        binding = FragmentListBinding.inflate(inflater, container, false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        tasksAdapter = new TasksAdapter(new TasksAdapter.ContactDiff(), ListFragment.this);
        binding.recyclerListTasks.setAdapter(tasksAdapter);
        binding.recyclerListTasks.setLayoutManager(new LinearLayoutManager(getContext()));
//        binding.recyclerListTasks.addItemDecoration(new DividerItemDecoration(this.getContext(), DividerItemDecoration.VERTICAL));
        tasksAdapter.submitList(taskService.getTasks());
    }
}