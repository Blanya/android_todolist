package com.example.todolist.adapter;

import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.ListAdapter;

import com.example.todolist.model.Task;
import com.example.todolist.viewholder.TaskViewHolder;

public class TasksAdapter extends ListAdapter<Task, TaskViewHolder> {
    private Fragment fragment;

    public TasksAdapter(@NonNull DiffUtil.ItemCallback<Task> diffCallback, Fragment fragment) {
        this(diffCallback);
        this.fragment = fragment;
    }
    public TasksAdapter(@NonNull DiffUtil.ItemCallback<Task> diffCallback) {
        super(diffCallback);
    }

    @NonNull
    @Override
    public TaskViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return TaskViewHolder.create(parent, fragment);
    }

    @Override
    public void onBindViewHolder(@NonNull TaskViewHolder holder, int position) {
        Task task = getItem(position);
        holder.display(task);
    }

    public static class ContactDiff extends DiffUtil.ItemCallback<Task> {

        @Override
        public boolean areItemsTheSame(@NonNull Task oldItem, @NonNull Task newItem) {
            return oldItem == newItem;
        }

        @Override
        public boolean areContentsTheSame(@NonNull Task oldItem, @NonNull Task newItem) {
            return oldItem.getTitle().equals(newItem.getTitle()) && oldItem.getDate().equals(newItem.getDate()) && oldItem.getDone().equals(newItem.getDone());
        }
    }
}