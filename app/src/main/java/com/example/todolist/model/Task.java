package com.example.todolist.model;

import java.io.Serializable;
import java.util.Date;

public class Task implements Serializable {
    private String title;
    private Boolean done;
    private Date date;
    private String description;

    public Task(String title, Date date) {
        this.title = title;
        this.done = false;
        this.date = date;
    }

    public Task(String title, Date date, String description) {
        this.title = title;
        this.done = false;
        this.date = date;
        this.description = description;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getDone() {
        return done;
    }

    public void setDone(Boolean done) {
        this.done = done;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
