package com.example.todolist.service;

import com.example.todolist.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskService {
    private static List<Task> tasks = new ArrayList<>();

    public void add(Task task){
        tasks.add(task);
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public Task getTaskByIndex(int id){
        return tasks.get(id);
    }

    public Task changeState(int id){
        Task task = getTaskByIndex(id);
        task.setDone(!task.getDone());
        return task;
    }

    public void deleteTask(int id){
        tasks.remove(id);
    }

}
