package com.example.todolist.viewholder;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.fragment.NavHostFragment;
import androidx.recyclerview.widget.RecyclerView;

import com.example.todolist.ListFragmentDirections;
import com.example.todolist.R;
import com.example.todolist.model.Task;
import com.example.todolist.service.TaskService;

import java.text.SimpleDateFormat;
import java.util.Date;

public class TaskViewHolder extends RecyclerView.ViewHolder {
    private Fragment fragment;
    private TextView titleTextView;
    private TextView dateTextView;
    private Button itemButton;
    private CheckBox checkBoxItem;
    private TaskService taskService = new TaskService();

    public TaskViewHolder(@NonNull View itemView, Fragment fragment) {
        this(itemView);
        this.fragment = fragment;
    }

    public TaskViewHolder(@NonNull View itemView) {
        super(itemView);
        titleTextView = itemView.findViewById(R.id.title_item);
//        itemButton = itemView.findViewById(R.id.button_item);
        dateTextView = itemView.findViewById(R.id.date_item);
        checkBoxItem = itemView.findViewById(R.id.checkbox_item);
    }

    public void display(Task task) {
        titleTextView.setText(task.getTitle());
        if(task.getDate() != null){
            String date = new SimpleDateFormat("dd.MM.yyyy").format(task.getDate());
            dateTextView.setText("DEADLINE: " + date);
        }else {
            dateTextView.setText("NO DEADLINE");
        }

        changeState(checkBoxItem, task);

        checkBoxItem.setOnClickListener((e) -> {
            taskService.changeState(getAdapterPosition());
            changeState(checkBoxItem, task);
//            changeBtn(itemButton, task);
        });


        itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                System.out.println(getAdapterPosition());
                NavDirections navDirections = com.example.todolist.ListFragmentDirections.actionListFragmentToDetailFragment(getAdapterPosition());
                NavHostFragment.findNavController(fragment).navigate(navDirections);
            }
        });
    }

//    public void changeBtn(Button itemButton, Task task){
//        if(task.getDone())
//            itemButton.setText("To do");
//        else
//            itemButton.setText("Done");
//    }

    public void changeState(CheckBox checkBox, Task task){
        checkBox.setChecked(task.getDone());
    }

    public static TaskViewHolder create(ViewGroup parent, Fragment fragment) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_item, parent, false);
        return new TaskViewHolder(view, fragment);
    }
}

